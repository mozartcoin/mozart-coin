Sample configuration files for:

SystemD: mozartcoind.service
Upstart: mozartcoind.conf
OpenRC:  mozartcoind.openrc
         mozartcoind.openrcconf
CentOS:  mozartcoind.init

have been made available to assist packagers in creating node packages here.

See doc/init.md for more information.
