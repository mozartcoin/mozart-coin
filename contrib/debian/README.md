
Debian
====================
This directory contains files used to package mozartcoind/mozartcoin-qt
for Debian-based Linux systems. If you compile mozartcoind/mozartcoin-qt yourself, there are some useful files here.

## mozartcoin: URI support ##


mozartcoin-qt.desktop  (Gnome / Open Desktop)
To install:

	sudo desktop-file-install mozartcoin-qt.desktop
	sudo update-desktop-database

If you build yourself, you will either need to modify the paths in
the .desktop file or copy or symlink your mozartcoinqt binary to `/usr/bin`
and the `../../share/pixmaps/mozartcoin128.png` to `/usr/share/pixmaps`

mozartcoin-qt.protocol (KDE)

