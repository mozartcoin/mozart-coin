Mozart Coin (WAM) integration/staging repository
======================================


Quick installation of the Mozart Coin daemon under linux.

Installation of libraries (using root user):

    add-apt-repository ppa:bitcoin/bitcoin -y
    apt-get update
    apt-get install -y build-essential libtool autotools-dev automake pkg-config libssl-dev libevent-dev bsdmainutils
    apt-get install -y libboost-system-dev libboost-filesystem-dev libboost-chrono-dev libboost-program-options-dev libboost-test-dev libboost-thread-dev
    apt-get install -y libdb4.8-dev libdb4.8++-dev

Cloning the repository and compiling (use any user with the sudo group):

    cd
    git clone https://github.com/Mozart-Coin/Mozart-Coin.git
    cd MozartCoin
    ./autogen.sh
    ./configure
    sudo make install
    cd src
    sudo strip mozartcoind
    sudo strip mozartcoin-cli
    sudo strip mozartcoin-tx
    cd ..

Running the daemon:

    mozartcoind 

Stopping the daemon:

    mozartcoin-cli stop

Demon status:

    mozartcoin-cli getinfo
    mozartcoin-cli mnsync status

P2P port: 11756, RPC port: 17561
---
Distributed under the MIT software license, see the accompanying file COPYING or http://www.opensource.org/licenses/mit-license.php.
